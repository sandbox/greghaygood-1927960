WiKID 2-Factor Authentication for  7


INSTALLATION:

Install like a normal Drupal module.  You'll need to create a network client on your WiKID server, then copy the certificate (PEM-encoded) to your Drupal server(s). Then visit the WiKID admin settings page to input all the relevant information.

Administration » Configuration » People » WiKID Authentication

No other configuration is needed!   Users can submit their PIN in the standard Drupal password field.  The credentials will first be checked against the local Drupal DB (for administrative failsafe purposes), then will be checked against the configured WiKID server.  Once authentication, the user account will be created in Drupal, if needed.


TODO:

- Look into encrypting the certificate passphrase if a relevant module such as Encrypt or AES is installed.
- Add option to override the domain code for a particular user.

