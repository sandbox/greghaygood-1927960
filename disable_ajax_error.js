/**
 * @file Two-factor authentication for Drupal.

   work around for annoying AJAX error in D7
   pulled from comments #98 and #125 on http://drupal.org/node/1232416

 */


(function($){
  Drupal.behaviors.wikid = (function(){
    return {
      attach:function(context, settings){
        $.ajaxSetup({
          beforeSend: function(jqXHR, settings) {
            settings.error = function(jqXHR, textStatus, errorThrown) {
            //do nothing... end user does not need to see debugging info, uncomment console log code to debug
            //{console.log(\'ajax error: \' + textStatus);};
            };
          }
        });
      }
    }
  })();

  Drupal.behaviors.ajaxHijackErrors = {
    attach:function(context, settings){
      if (typeof context !== 'undefined') { //run only if there is a context var
        window.console = window.console || {};
        var methods = ['log', 'warn', 'error'];
        for (var i = 0; i < methods.length; i++) {
          window.console[methods[i]] = window.console[methods[i]] || function() {};
        } //end for

        $.ajaxSetup({
          beforeSend: function(jqXHR, settings) {
            settings.error = function(jqXHR, textStatus, errorThrown) {
              //end user doesn't need to see debugging info
              {console.log('ajax error: ' + textStatus);};
            }; //end settings.error
          } //end beforeSend
        }); //end $.ajaxSetup
      } // end if (typeof context !== 'undefined')
    } // end attach:function(context, settings)
  } //end Drupal.behaviors.ajaxHijackErrors

})(jQuery);

