<?php
/**
 * @file
 * Contains the administration interface for the WiKID Authentication Module.
 * The user may chose where to display the module and setup it's behavior.
 */

/**
 * Displays the settings page.
 */
function wikid_admin_settings($form, &$form_state) {

  // work around for annoying AJAX error in D7
  drupal_add_js(drupal_get_path('module', 'wikid') . '/disable_ajax_error.js');

  // Read Settings.
//  $settings = wikid_get_settings();

  // Server settings
  $form['wikid_server'] = array(
    '#type' => 'fieldset',
    '#title' => t('WiKID Server'),
    '#id' => 'wikid_server',
  );

  // Server Host
  $form['wikid_server']['wikid_server_host'] = array(
    '#id' => 'wikid_server_host',
    '#type' => 'textfield',
    '#title' => t('IP or Hostname'),
    '#default_value' => variable_get('wikid_server_host', '127.0.0.1'),
    '#description' => t('The IP (preferably) or hostname of your WiKID authentication server.'),
    '#size' => 60,
    '#maxlength' => 60,
  );

  // wAuth Port
  $form['wikid_server']['wikid_server_port'] = array(
    '#id' => 'wikid_server_port',
    '#type' => 'textfield',
    '#title' => t('Port'),
    '#default_value' => variable_get('wikid_server_port', '8388'),
    '#description' => t('The wAuth port of your WiKID server, if different from the default.'),
    '#size' => 60,
    '#maxlength' => 60,
  );

  // Domaincode
  $form['wikid_server']['wikid_domaincode'] = array(
    '#id' => 'wikid_domaincode',
    '#type' => 'textfield',
    '#title' => t('Domain Code'),
    '#default_value' => variable_get('wikid_domaincode', '127000000001'),
    '#description' => t('The WiKID domain code to use for this Drupal site.  (In a future release, this code can be overridden for a particular user.)'),
    '#size' => 60,
    '#maxlength' => 60,
  );

  $form['wikid_nc_certificate'] = array(
    '#type' => 'fieldset',
    '#title' => t('WiKID Network Client Certificate'),
    '#id' => 'wikid_nc_certificate',
  );

  // Certificate Path
  $form['wikid_nc_certificate']['wikid_keyfile'] = array(
    '#id' => 'wikid_keyfile',
    '#type' => 'textfield',
    '#title' => t('Certificate File Location'),
    '#default_value' => variable_get('wikid_keyfile', ''),
    '#description' => t('The path and name of the PEM-encoded network client certificate to use for this Drupal server.  If the path does not start with a /, then the file will be assumed to be with the module files in<br/> @module_path.',
        array('@module_path' => dirname(__FILE__))),
    '#size' => 90,
    '#maxlength' => 255,
  );

  // Certificate Passphrase
  $form['wikid_nc_certificate']['wikid_keypass'] = array(
    '#id' => 'wikid_keypass',
    '#type' => 'textfield',
    '#title' => t('Certificate Passphrase'),
    '#default_value' => variable_get('wikid_keypass', ''),
    '#description' => t('The passphrase to unlock the network client certificate.  <b>Note</b>: this passphrase will be stored unencrypted in your Drupal database.'),
    '#size' => 60,
    '#maxlength' => 60,
  );

  // Check communication with server
  $form['wikid_check_server']['verify'] = array(
    '#id' => 'wikid_check_communication_button',
    '#type' => 'button',
    '#value' => t('Verify Server Settings'),
    '#weight' => 1,
    '#ajax' => array(
      'callback' => 'wikid_admin_ajax_check_communication_settings',
      'wrapper' => 'wikid_server',
      'method' => 'replace',
      'effect' => 'fade',
    ),
  );

  $form['wikid_options'] = array(
    '#type' => 'fieldset',
    '#title' => t('Site Configuration Options'),
    '#id' => 'wikid_options',
  );

  // Allow override?
  $form['wikid_options']['wikid_allow_domaincode_override'] = array(
    '#id' => 'wikid_allow_domaincode_override',
    '#type' => 'checkbox',
    '#title' => t('Allow domain code to be overridden per user?'),
    '#default_value' => variable_get('wikid_allow_domaincode_override', '0'),
    '#description' => t('(Future use) Allow a user to authentication against a different WiKID server than the site default?'),
    '#size' => 60,
    '#disabled' => TRUE,
    '#maxlength' => 60,
  );

  $form['#submit'][] = 'wikid_admin_settings_submit';

  return system_settings_form($form);
}

/**
 * Callback Handler to verify the Communication Settings.
 */
function wikid_admin_ajax_check_communication_settings($form, &$form_state) {

  $module_path = dirname(__FILE__);


  // Sanitize data.
  $server_host = (isset($form_state['values']['wikid_server_host']) ? trim(drupal_strtolower($form_state['values']['wikid_server_host'])) : '');
  $server_port = (isset($form_state['values']) ? trim($form_state['values']['wikid_server_port']) : '8388');

  $client_key_file = (isset($form_state['values']['wikid_keyfile']) ? trim(drupal_strtolower($form_state['values']['wikid_keyfile'])) : '');
  if ($client_key_file[0] != '/') {
    $client_key_file = $module_path . '/' . $client_key_file;
  }
  $client_key_pass = (isset($form_state['values']['wikid_keypass']) ? trim($form_state['values']['wikid_keypass']) : '');

  // Message to be shown.
  $error_message = '';
  $success_message = '';

  // Some fields are empty.
  if (empty($server_host) || empty($server_port)) {
    $error_message = t('Please fill out the server settings');
  }
  // All fields has been filled out.
  else {

    require_once( $module_path . '/wClient.inc.php' );
    $ca_file = $module_path . '/WiKID-ca.pem';

    $wc = new wClient($server_host, $server_port, $client_key_file, $client_key_pass, $ca_file);
    if (!$wc) {
      $error_message = t('Unable to communicate with WiKID server');
    }
    $domains = $wc->getDomains();
    error_log($domains);
    $domaincode_list = array();
    //dsm($domains);
    foreach ($domains as $domain) {
      $label = $domain->name . " (" . $domain->domaincode . ")";
      $domaincode_list[$domain->domaincode] = $label;
    }
    //dsm($domaincode_list);

    if (!is_array($domaincode_list)) {
      $error_message = t('Unable to communicate with WiKID server');
    }
    else {
      $success_message = t('The settings are correct - do not forget to save your changes!');
    }
  }

  // Error.
  if (!empty($success_message)) {
    drupal_set_message(check_plain($success_message), 'status wikid');
  }
  else {
    drupal_set_message($error_message, 'error wikid');
  }
  return $form['wikid_server'];
}

/**
 * Saves the administration area settings.
 */
function wikid_admin_settings_submit($form, &$form_state) {

  // Remove Drupal stuff.
  form_state_values_clean($form_state);

  error_log('saving settings ....');
  print_r($form_state['values']);

  // Save values.
  foreach ($form_state['values'] as $setting => $value) {

    $value = trim($value);

    // server host
    if ($setting == 'wikid_server_host') {
      // The server host is always in lower-case
      $value = drupal_strtolower($value);
    }
    elseif ($setting == 'wikid_keyfile' && $value[0] != '/') {
      $value = dirname(__FILE__) . '/' . $value;
    }

    $form_state['values'][$setting] = $value;

  }
  //drupal_set_message(t('Settings saved successfully'), 'status wikid');
  cache_clear_all();
  menu_rebuild();
}
